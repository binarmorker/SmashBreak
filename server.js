const express = require("express");
const server = express();
const fs = require("fs");
const path = require("path");
const bodyParser = require("body-parser");
const cors = require("cors");
const morgan = require("morgan");
const helmet = require("helmet");
const axios = require("axios");
const bundle = require("./dist/server.bundle.js");
const renderer = require("vue-server-renderer").createRenderer({
  template: fs.readFileSync("./src/index.html", "utf-8")
});

server.use(helmet());
server.use(morgan("combined"));
server.use(bodyParser.json());
server.use(cors());

const API = (() => {
  const url = "http://smash4.logitar.com/";

  return {
    characters: async (key, communityId) =>
      axios.get(url + "characters", {
        headers: { "X-Api-Key": key },
        params: { community: communityId }
      }),
    matches: async (key, communityId) =>
      axios.get(url + "match", {
        headers: { "X-Api-Key": key },
        params: { community: communityId }
      }),
    match: async (key, communityId, id) =>
      axios.get(url + "match", {
        headers: { "X-Api-Key": key },
        params: { community: communityId, id: id }
      }),
    stages: async (key, communityId) =>
      axios.get(url + "stages", {
        headers: { "X-Api-Key": key },
        params: { community: communityId }
      })
  };
})();

let data = {};

const communityId = 1;
const apiKey =
  "1065d7565afc42e6975f03defc1c127ad8d303e461aa4a0121240dad76da4a24";

server.use("/dist", express.static(path.join(__dirname, "./dist")));

server.get("/api/matches", (req, res, next) => {
  res.setHeader("Content-Type", "application/json");
  API.matches(apiKey, communityId)
    .then(x => res.send(x.data))
    .catch(next);
});
server.get("/api/match/:id", (req, res, next) => {
  res.setHeader("Content-Type", "application/json");
  API.match(apiKey, communityId, Number(req.params.id))
    .then(x => res.send(x.data))
    .catch(next);
});
server.get("/api/data", (req, res, next) => {
  res.setHeader("Content-Type", "application/json");
  if (!data.characters && !data.stages) {
    Promise.all([
      API.characters(apiKey, communityId),
      API.stages(apiKey, communityId)
    ])
      .then(x => {
        data = { characters: x[0].data, stages: x[1].data };
        res.send(data);
      })
      .catch(next);
  } else {
    res.send(data);
  }
});

server.get("*", function(req, res) {
  bundle.default({ url: req.url }).then(
    app => {
      renderer.renderToString(app, (err, html) => {
        if (err) {
          if (err.code === 404) {
            res.status(404).send("Page not found");
          } else {
            res.status(500).send("Internal server error");
          }
        } else {
          //res.setHeader("Content-Type", "text/html");
          res.send(html);
        }
      });
    },
    err => {
      console.log(err);
    }
  );
});

server.listen(8080);
