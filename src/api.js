const baseUrl =
  process.env.NODE_ENV === "production" ? "" : "http://localhost:8081";

export default {
  getMatches: baseUrl + "/api/matches",
  getMatch: baseUrl + "/api/match/",
  getData: baseUrl + "/api/data"
};
