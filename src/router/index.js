import Vue from "vue";
import Router from "vue-router";
import MatchList from "../views/MatchList";
import MatchPage from "../views/MatchPage";
import CharacterList from "../views/CharacterList";
import AboutPage from "../views/AboutPage";
import TermsPage from "../views/TermsPage";
import PrivacyPage from "../views/PrivacyPage";

Vue.use(Router);

export function createRouter() {
  return new Router({
    mode: "history",
    base: process.env.BASE_URL,
    scrollBehavior(to, from, savedPosition) {
      if (savedPosition) {
        return savedPosition;
      } else {
        return { x: 0, y: 0 };
      }
    },
    routes: [
      {
        path: "/",
        name: "home",
        component: MatchList
      },
      {
        path: "/match/:id",
        name: "match",
        component: MatchPage
      },
      {
        path: "/characters",
        name: "characters",
        component: CharacterList
      },
      {
        path: "/about",
        name: "about",
        component: AboutPage
      },
      {
        path: "/terms",
        name: "terms",
        component: TermsPage
      },
      {
        path: "/privacy",
        name: "privacy",
        component: PrivacyPage
      }
    ]
  });
}
