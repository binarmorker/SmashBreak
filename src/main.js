import "babel-polyfill";
import Vue from "vue";
import Vuex from "vuex";
import VueI18n from "vue-i18n";
import App from "./App.vue";
import { createRouter } from "./router";
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue/dist/bootstrap-vue.css";
import { library } from "@fortawesome/fontawesome-svg-core";
import { faExternalLinkAlt, faHeart } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";
import store from "./store";
import enLang from "./i18n/en";

library.add(faExternalLinkAlt);
library.add(faHeart);
Vue.component("fa", FontAwesomeIcon);

Vue.use(Vuex);
Vue.use(VueI18n);
Vue.config.productionTip = false;

export function createApp() {
  const router = createRouter();

  const app = new Vue({
    i18n: new VueI18n({
      locale: "en",
      messages: {
        en: enLang
      }
    }),
    router,
    store: store(),
    render: h => h(App)
  });

  return { app, router };
}
