import { Store } from "vuex";
import matchModule from "./match";
import axios from "axios";
import api from "../api";

const state = {
  characters: [],
  stages: []
};

const mutations = {
  setCharacters: (state, characters) => {
    state.characters = characters;
  },
  setStages: (state, stages) => {
    state.stages = stages;
  }
};

const actions = {
  loadData: async ({ commit }) => {
    const response = await axios.get(api.getData);
    commit("setCharacters", response.data.characters);
    commit("setStages", response.data.stages);
  }
};

const getters = {
  getCharacters: state => state.characters,
  getStages: state => state.stages
};

export default () =>
  new Store({
    modules: {
      match: matchModule
    },
    mutations,
    state,
    actions,
    getters
  });
