import axios from "axios";
import api from "../../api";

const state = {
  matches: {},
  matchList: []
};

const mutations = {
  setMatch: (state, [id, match]) => {
    state.matches[id] = match;
  },
  setMatches: (state, matches) => {
    state.matchList = matches;
  }
};

const actions = {
  loadMatch: async ({ commit }, id) => {
    const match = await axios.get(api.getMatch + id);
    commit("setMatch", [id, match.data]);
  },
  loadMatchList: async ({ commit }) => {
    const matches = await axios.get(api.getMatches);
    commit("setMatches", matches.data);
  }
};

const getters = {
  getMatch: state => id => state.matches[id],
  getMatchList: state => state.matchList
};

export default {
  state,
  mutations,
  actions,
  getters
};
