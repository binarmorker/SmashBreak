export default {
  characters: {
    Bayonetta: "Bayonetta",
    Bowser: "Bowser",
    BowserJr: "Bowser Jr.",
    CaptainFalcon: "Captain Falcon",
    Charizard: "Charizard",
    Cloud: "Cloud",
    Corrin: "Corrin",
    DarkPit: "Dark Pit",
    DiddyKong: "Diddy Kong",
    DonkeyKong: "Donkey Kong",
    DrMario: "Dr. Mario",
    DuckHunt: "Duck Hunt",
    Falco: "Falco",
    Fox: "Fox",
    Ganondorf: "Ganondorf",
    Greninja: "Greninja",
    Ike: "Ike",
    Jigglypuff: "Jigglypuff",
    KingDedede: "King Dedede",
    Kirby: "Kirby",
    Link: "Link",
    LittleMac: "Little Mac",
    Lucario: "Lucario",
    Lucas: "Lucas",
    Lucina: "Lucina",
    Luigi: "Luigi",
    Mario: "Mario",
    Marth: "Marth",
    MegaMan: "Megaman",
    MetaKnight: "Meta Knight",
    Mewtwo: "Mewtwo",
    MiiBrawler: "Mii Brawler",
    MiiGunner: "Mii Gunner",
    MiiSwordfighter: "Mii Swordfighter",
    MrGameWatch: "Mr. Game & Watch",
    Ness: "Ness",
    Olimar: "Olimar",
    PacMan: "Pac-Man",
    Palutena: "Palutena",
    Peach: "Peach",
    Pikachu: "Pikachu",
    Pit: "Pit",
    ROB: "R.O.B.",
    Robin: "Robin",
    RosalinaLuma: "Rosalina & Luma",
    Roy: "Roy",
    Ryu: "Ryu",
    Samus: "Samus",
    Sheik: "Sheik",
    Shulk: "Shulk",
    Sonic: "Sonic",
    ToonLink: "Toon Link",
    undefined: "Unknown",
    Villager: "Villager",
    Wario: "Wario",
    WiiFitTrainer: "Wii Fit Trainer",
    Yoshi: "Yoshi",
    Zelda: "Zelda",
    ZeroSuitSamus: "Zero Suit Samus"
  }
  /*,
  'stages': [{'Code': 'Battlefield', 'StageID': 1}, {'Code': 'BigBattlefield', 'StageID': 2}, {
    'Code': 'BoxingRing',
    'StageID': 3
  }, {'Code': 'Coliseum', 'StageID': 4}, {'Code': 'DuckHunt', 'StageID': 5}, {
    'Code': 'FinalDestination',
    'StageID': 6
  }, {'Code': 'FlatZoneX', 'StageID': 7}, {'Code': 'Gamer', 'StageID': 8}, {
    'Code': 'GardenOfHope',
    'StageID': 9
  }, {'Code': 'GaurPlain', 'StageID': 10}, {'Code': 'JungleHijinxs', 'StageID': 11}, {
    'Code': 'KalosPokémonLeague',
    'StageID': 12
  }, {'Code': 'MarioCircuit', 'StageID': 13}, {'Code': 'MarioGalaxy', 'StageID': 14}, {
    'Code': 'Midgar',
    'StageID': 15
  }, {'Code': 'Miiverse', 'StageID': 16}, {'Code': 'MushroomKingdomU', 'StageID': 17}, {
    'Code': 'OrbitalGateAssault',
    'StageID': 18
  }, {'Code': 'PacLand', 'StageID': 19}, {'Code': 'PalutenaTemple', 'StageID': 20}, {
    'Code': 'Pilotwings',
    'StageID': 21
  }, {'Code': 'Pyrosphere', 'StageID': 22}, {'Code': 'Skyloft', 'StageID': 23}, {
    'Code': 'SSB64_DreamLand',
    'StageID': 24
  }, {'Code': 'SSB64_HyruleCastle64', 'StageID': 25}, {
    'Code': 'SSB64_KongoJungle64',
    'StageID': 26
  }, {'Code': 'SSB64_PeachCastle', 'StageID': 27}, {'Code': 'SSBB_75m', 'StageID': 28}, {
    'Code': 'SSBB_BridgeOfEldin',
    'StageID': 29
  }, {'Code': 'SSBB_CastleSiege', 'StageID': 30}, {'Code': 'SSBB_DelfinoPlaza', 'StageID': 31}, {
    'Code': 'SSBB_Halberd',
    'StageID': 32
  }, {'Code': 'SSBB_LuigiMansion', 'StageID': 33}, {
    'Code': 'SSBB_LylatCruise',
    'StageID': 34
  }, {'Code': 'SSBB_MarioCircuit', 'StageID': 35}, {'Code': 'SSBB_Norfair', 'StageID': 36}, {
    'Code': 'SSBB_PirateShip',
    'StageID': 37
  }, {'Code': 'SSBB_PokémonStadium2', 'StageID': 38}, {
    'Code': 'SSBB_PortTownAeroDive',
    'StageID': 39
  }, {'Code': 'SSBB_Skyworld', 'StageID': 40}, {'Code': 'SSBB_Smashville', 'StageID': 41}, {
    'Code': 'SSBM_Onett',
    'StageID': 42
  }, {'Code': 'SSBM_Temple', 'StageID': 43}, {'Code': 'SSBM_YoshiIsland', 'StageID': 44}, {
    'Code': 'SuperMarioMaker',
    'StageID': 45
  }, {'Code': 'SuzakuCastle', 'StageID': 46}, {'Code': 'TheGreatCaveOffensive', 'StageID': 47}, {
    'Code': 'TownAndCity',
    'StageID': 48
  }, {'Code': 'UmbraClockTower', 'StageID': 49}, {'Code': 'WiiFitStudio', 'StageID': 50}, {
    'Code': 'WilyCastle',
    'StageID': 51
  }, {'Code': 'WindyHillZone', 'StageID': 52}, {'Code': 'WoollyWorld', 'StageID': 53}, {
    'Code': 'WreckingCrew',
    'StageID': 54
  }, {'Code': 'WuhuIsland', 'StageID': 55}]
}*/
};
